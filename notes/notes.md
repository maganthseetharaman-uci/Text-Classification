# To Do

- [HIGH] Implement library for `TF-IDF`, `word2vec`
- [LOW] Look for other embeddings 
    - Google and Facebook

# Notes

## Ignorning Lemmatization
- Ignoring lemmatizaiton as the increased performance to increased cost ratio is quite low.
    - Refer: https://stats.stackexchange.com/a/523076